﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login_Application_for_OOP2_HW1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            label4.TextAlign = ContentAlignment.MiddleCenter;
            label4.Text = Calculator.Add(Convert.ToInt32(textBox1.Text), Convert.ToInt32(textBox2.Text)).ToString();
            CheckBackColorOfResult();
            CheckBackColorOfButtons(button1);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            label4.TextAlign = ContentAlignment.MiddleCenter;
            label4.Text = Calculator.Substract(Convert.ToInt32(textBox1.Text), Convert.ToInt32(textBox2.Text)).ToString();
            CheckBackColorOfResult();
            CheckBackColorOfButtons(button2);
        }
        private void button3_Click(object sender, EventArgs e)
        {
            CheckBackColorOfButtons(button3);
            try
            {
                label4.TextAlign = ContentAlignment.MiddleCenter;
                label4.Text = Calculator.Divide(Convert.ToDouble(textBox1.Text), Convert.ToDouble(textBox2.Text)).ToString();
                CheckBackColorOfResult();
            }
            catch (DivideByZeroException x)
            {
                label4.Text = x.Message;
            }
            
        }
        private void button4_Click(object sender, EventArgs e)
        {
            label4.TextAlign = ContentAlignment.MiddleCenter;
            label4.Text = Calculator.Multiply(Convert.ToInt32(textBox1.Text), Convert.ToInt32(textBox2.Text)).ToString();
            CheckBackColorOfResult();
            CheckBackColorOfButtons(button4);
        }
        private void CheckBackColorOfResult()
        {
            if (Convert.ToDouble(label4.Text) > 0)
            {
                label4.BackColor = Color.Green;
            }
            else
            {
                label4.BackColor = Color.Red;
            }
        }
        private void CheckBackColorOfButtons(Button button)
        {
            button1.BackColor = Color.Gray;
            button2.BackColor = Color.Gray;
            button3.BackColor = Color.Gray;
            button4.BackColor = Color.Gray;
            button.BackColor = Color.Blue;

        }
        private void button1_MouseHover(object sender, EventArgs e)
        {
            button1.ForeColor = Color.Red;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.ForeColor = Color.Black;
        }

        private void button2_MouseHover(object sender, EventArgs e)
        {
            button2.ForeColor = Color.Red;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.ForeColor = Color.Black;
        }

        private void button4_MouseHover(object sender, EventArgs e)
        {
            button4.ForeColor = Color.Red;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            button4.ForeColor = Color.Black;
        }

        private void button3_MouseHover(object sender, EventArgs e)
        {
            button3.ForeColor = Color.Red;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.ForeColor = Color.Black;
        }
    }
}
