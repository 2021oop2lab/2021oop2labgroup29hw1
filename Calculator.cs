﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login_Application_for_OOP2_HW1
{
    public static class Calculator
    {
        public static int Add(int number1,int number2)
        {
            return number1 + number2;
        }
        public static int Substract(int number1, int number2)
        {
            return number1 - number2;
        }
        public static int Multiply(int number1, int number2)
        {
            return number1 * number2;
        }
        public static double Divide (double number1,double number2)
        {
            if (number2 != 0)
            {
                return number1 / number2;
            }
            else
            {
                throw new DivideByZeroException();
            }

        }

    }
}
