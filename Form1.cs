﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login_Application_for_OOP2_HW1
{
    public partial class windowLogin : Form
    {

        User[] users = new User[10];

        public windowLogin()
        {
            InitializeComponent();
            for (int i = 0; i < users.Length; i++) users[i] = new User();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            users[0] = new User("ali22", "123");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool ver1 = false, ver2 = false;

            for (int i = 0; i < 10; i++)
            {
                if (users[i].checkUsername(txtUsername.Text)) ver1 = true;
                if (users[i].checkPassword(txtPassword.Text)) ver2 = true;
            }

            if (ver1 && ver2 == true)
            {
                lblLoginResult.TextAlign = ContentAlignment.MiddleCenter;
                lblLoginResult.ForeColor = Color.YellowGreen;
                lblLoginResult.Text = "Login succeeded! Taking you to windowCalc...";
                lblLoginResult.Refresh();
                System.Threading.Thread.Sleep(3000);
                this.Hide();
                Form2 calculator = new Form2();
                calculator.Closed += (s, args) => this.Close();
                calculator.Show();
            }

            else
            {
                lblLoginResult.TextAlign = ContentAlignment.MiddleCenter;
                lblLoginResult.ForeColor = Color.Red;
                lblLoginResult.Text = "Login failed! Please try again.";
            }
        }



        private void lblLoginResult_Click(object sender, EventArgs e)
        {

        }
    }
}
