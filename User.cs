﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login_Application_for_OOP2_HW1
{
    class User
    {
        private string username;
        private string password;

        public User()
        {
            username = "admin";
            password = "admin";
        }

        public User(string _username, string _password)
        {
            username = _username;
            password = _password;
        }

        public bool checkUsername(string _username)
        {
            if (username == _username) return true;
            else return false;
        }

        public bool checkPassword(string _password)
        {
            if (password == _password) return true;
            else return false;
        }

        public void setUsername(string _username)
        {
            username = _username;
        }

        public void setPassword(string _password)
        {
            password = _password;
        }

    }
}
